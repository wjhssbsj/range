package main

import "fmt"

type Abnormal struct {
	reason string
}

func (A *Abnormal) Error() string {
	return A.reason
}

type RangeNode struct {
	begin int
	end   int
	next  *RangeNode
}

func (rangeNode *RangeNode) New(begin, end int) *RangeNode {
	return &RangeNode{begin: begin, end: end}
}

func (rangeNode *RangeNode) isUseless(begin, end int) bool {
	return begin == end
}

func (rangeNode *RangeNode) isEqual(begin, end int) bool {
	return begin == rangeNode.begin && end == rangeNode.end
}

func (rangeNode *RangeNode) isLeftOuter(begin, end int) bool {
	return end < rangeNode.begin
}

func (rangeNode *RangeNode) isLeftInner(begin, end int) bool {
	return begin <= rangeNode.begin && end < rangeNode.end && end >= rangeNode.begin
}

func (rangeNode *RangeNode) isRightOuter(begin, end int) bool {
	return begin > rangeNode.end
}

func (rangeNode *RangeNode) isRightInner(begin, end int) bool {
	return begin > rangeNode.begin && begin <= rangeNode.end && end >= rangeNode.end
}

func (rangeNode *RangeNode) isInside(begin, end int) bool {
	return begin > rangeNode.begin && end < rangeNode.end
}

func (rangeNode *RangeNode) isOutside(begin, end int) bool {
	return begin <= rangeNode.begin && end >= rangeNode.end
}

func (rangeNode *RangeNode) isNull() bool {
	return rangeNode == nil
}

func (rangeNode *RangeNode) insertRight(end int) *RangeNode {
	for p, n := rangeNode, rangeNode.next; n != nil; p, n = n, n.next {
		p.next = nil
		if n.begin > end {
			p.next = nil
			rangeNode.next = n
			return rangeNode
		}
		if n.end >= end {
			rangeNode.end = n.end
			rangeNode.next = n.next
			n.next = nil
			return rangeNode
		}
	}
	return rangeNode
}

func (rangeNode *RangeNode) deleteRight(end int) *RangeNode {
	for p, n := rangeNode, rangeNode.next; n != nil; p, n = n, n.next {
		p.next = nil
		if n.begin > end {
			return n
		}
		if n.end > end {
			n.begin = end
			return n
		}
	}
	return nil
}

func (rangeNode *RangeNode) InsertSection(begin, end int) *RangeNode {
	if rangeNode.isNull() {
		return rangeNode.New(begin, end)
	}
	if rangeNode.isUseless(begin, end) {
		return rangeNode
	}
	if rangeNode.isInside(begin, end) {
		return rangeNode
	}
	if rangeNode.isEqual(begin, end) {
		return rangeNode
	}
	if rangeNode.isLeftOuter(begin, end) {
		newNode := rangeNode.New(begin, end)
		newNode.next = rangeNode
		return newNode
	}
	if rangeNode.isLeftInner(begin, end) {
		rangeNode.begin = begin
		return rangeNode
	}
	if rangeNode.isRightInner(begin, end) {
		rangeNode.end = end
		rangeNode = rangeNode.insertRight(end)
	}
	if rangeNode.isOutside(begin, end) {
		rangeNode.begin = begin
		rangeNode.end = end
		rangeNode = rangeNode.insertRight(end)
		return rangeNode
	}
	if rangeNode.isRightOuter(begin, end) {
		if rangeNode.next.isNull() {
			rangeNode.next = rangeNode.New(begin, end)
			return rangeNode
		}
		rangeNode.next = rangeNode.next.InsertSection(begin, end)
		return rangeNode
	}
	return rangeNode
}

func (rangeNode *RangeNode) DeleteSection(begin, end int) *RangeNode {
	if rangeNode.isNull() {
		return rangeNode
	}
	if rangeNode.isUseless(begin, end) {
		return rangeNode
	}
	if rangeNode.isEqual(begin, end) {
		nextNode := rangeNode.next
		rangeNode.next = nil
		return nextNode
	}
	if rangeNode.isLeftOuter(begin, end) {
		return rangeNode
	}
	if rangeNode.isLeftInner(begin, end) {
		rangeNode.begin = end
		return rangeNode
	}
	if rangeNode.isInside(begin, end) {
		newNode := rangeNode.New(end, rangeNode.end)
		rangeNode.end = begin
		newNode.next = rangeNode.next
		rangeNode.next = newNode
		return rangeNode
	}
	if rangeNode.isRightInner(begin, end) {
		if !rangeNode.next.isNull() {
			rangeNode.next = rangeNode.next.DeleteSection(rangeNode.end, end)
		}
		rangeNode.end = begin
		return rangeNode
	}
	if rangeNode.isRightOuter(begin, end) {
		if !rangeNode.next.isNull() {
			rangeNode.next = rangeNode.next.DeleteSection(begin, end)
		}
		return rangeNode
	}
	if rangeNode.isOutside(begin, end) {
		if rangeNode.next.isNull() {
			return nil
		}
		return rangeNode.deleteRight(end)
	}
	return rangeNode
}

func (rangeNode *RangeNode) PrintValue() {
	for n := rangeNode; n != nil; n = n.next {
		fmt.Printf("[%d, %d) ", n.begin, n.end)
	}
	fmt.Printf("\r\n")
}

type RangeList struct {
	head *RangeNode
}

func (rangeList *RangeList) checkRangeElement(rangeElement [2]int) error {
	if rangeElement[0] > rangeElement[1] {
		reason := fmt.Sprintf("input error: [%d, %d)", rangeElement[0], rangeElement[1])
		return &Abnormal{reason: reason}
	}
	return nil
}

func (rangeList *RangeList) Add(rangeElement [2]int) error {
	if err := rangeList.checkRangeElement(rangeElement); err != nil {
		return err
	}
	rangeList.head = rangeList.head.InsertSection(rangeElement[0], rangeElement[1])
	return nil
}

func (rangeList *RangeList) Remove(rangeElement [2]int) error {
	if err := rangeList.checkRangeElement(rangeElement); err != nil {
		return err
	}
	rangeList.head = rangeList.head.DeleteSection(rangeElement[0], rangeElement[1])
	return nil
}

func (rangeList *RangeList) Print() error {
	rangeList.head.PrintValue()
	return nil
}

func main() {
	rl := RangeList{}
	rl.Add([2]int{1, 5})
	rl.Print()
	// Should display: [1, 5)
	rl.Add([2]int{10, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 21})
	rl.Print()
	// Should display: [1, 5) [10, 21)
	rl.Add([2]int{2, 4})
	rl.Print()
	// Should display: [1, 5) [10, 21)
	rl.Add([2]int{3, 8})
	rl.Print()
	// Should display: [1, 8) [10, 21)
	rl.Remove([2]int{10, 10})
	rl.Print()
	// Should display: [1, 8) [10, 21)
	rl.Remove([2]int{10, 11})
	rl.Print()
	// Should display: [1, 8) [11, 21)
	rl.Remove([2]int{15, 17})
	rl.Print()
	// Should display: [1, 8) [11, 15) [17, 21)
	rl.Remove([2]int{3, 19})
	rl.Print()
	// Should display: [1, 3) [19, 21)
}
