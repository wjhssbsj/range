package main

import (
	"testing"
)

func genRangeList(elements [][2]int) *RangeList {
	list := RangeList{}
	for _, element := range elements {
		list.Add(element)
	}
	return &list
}

func getRangeListValue(rangeList *RangeList) [][2]int {
	var result [][2]int
	if rangeList.head == nil {
		return result
	}
	for n := rangeList.head; n != nil; n = n.next {
		result = append(result, [2]int{n.begin, n.end})
	}
	return result
}

func isEqualValue(elements1, elements2 [][2]int) bool {
	if len(elements1) != len(elements2) {
		return false
	}
	if elements1 == nil || elements2 == nil {
		return false
	}
	for i := 0; i < len(elements1); i++ {
		e1 := elements1[i]
		e2 := elements2[i]
		if e1 != e2 {
			return false
		}
	}
	return true
}

func TestRangeList_InputError(t *testing.T) {
	list := genRangeList(nil)
	element := [2]int{6, 5}
	if err := list.Add(element); err == nil {
		t.Fail()
	}
	if err := list.Remove(element); err == nil {
		t.Fail()
	}
}

func TestRangeList_TestUseless(t *testing.T) {
	element := [2]int{1, 1}
	expect := [][2]int{{1, 5}}
	list := genRangeList(expect)
	list.Add(element)
	result := getRangeListValue(list)
	if !isEqualValue(result, expect) {
		t.Fail()
	}
	list.Remove(element)
	result = getRangeListValue(list)
	if !isEqualValue(result, expect) {
		t.Fail()
	}
}

func TestRangeList_AddInclude(t *testing.T) {
	initial := [][2]int{{1, 5}, {10, 15}, {20, 25}}
	list := genRangeList(initial)
	list.Add([2]int{2, 3})
	list.Add([2]int{10, 15})
	list.Add([2]int{20, 23})
	result := getRangeListValue(list)
	if !isEqualValue(result, initial) {
		t.Fail()
	}
}

func TestRangeList_AddLeftOuter(t *testing.T) {
	initial := [][2]int{{10, 15}, {20, 25}}
	list := genRangeList(initial)
	list.Add([2]int{1, 5})
	list.Add([2]int{17, 19})
	result := getRangeListValue(list)
	expect := [][2]int{{1, 5}, {10, 15}, {17, 19}, {20, 25}}
	if !isEqualValue(result, expect) {
		t.Fail()
	}
}

func TestRangeList_AddLeftInner(t *testing.T) {
	initial := [][2]int{{5, 10}, {15, 20}, {25, 30}}
	list := genRangeList(initial)
	list.Add([2]int{3, 7})
	list.Add([2]int{15, 17})
	list.Add([2]int{22, 25})
	result := getRangeListValue(list)
	expect := [][2]int{{3, 10}, {15, 20}, {22, 30}}
	if !isEqualValue(result, expect) {
		t.Fail()
	}
}

func TestRangeList_AddRightOuter(t *testing.T) {
	list := genRangeList(nil)
	list.Add([2]int{1, 5})
	list.Add([2]int{10, 20})
	result := getRangeListValue(list)
	expect := [][2]int{{1, 5}, {10, 20}}
	if !isEqualValue(result, expect) {
		t.Fail()
	}
}

func TestRangeList_AddRightInner(t *testing.T) {
	initial := [][2]int{{10, 15}, {20, 25}, {30, 35}}
	list := genRangeList(initial)
	list.Add([2]int{12, 17})
	list.Add([2]int{25, 27})
	list.Add([2]int{32, 35})
	result := getRangeListValue(list)
	expect := [][2]int{{10, 17}, {20, 27}, {30, 35}}
	if !isEqualValue(result, expect) {
		t.Fail()
	}
}

func TestRangeList_AddOutside(t *testing.T) {
	initial := [][2]int{{10, 15}, {20, 25}, {30, 35}}
	list := genRangeList(initial)
	list.Add([2]int{7, 17})
	result := getRangeListValue(list)
	expect := [][2]int{{7, 17}, {20, 25}, {30, 35}}
	if !isEqualValue(result, expect) {
		t.Fail()
	}
	list.Add([2]int{7, 24})
	result = getRangeListValue(list)
	expect = [][2]int{{7, 25}, {30, 35}}
	if !isEqualValue(result, expect) {
		t.Fail()
	}
	list.Add([2]int{6, 35})
	result = getRangeListValue(list)
	expect = [][2]int{{6, 35}}
	if !isEqualValue(result, expect) {
		t.Fail()
	}
}

func TestRangeList_RemoveEqual(t *testing.T) {
	initial := [][2]int{{10, 15}, {20, 25}, {30, 35}}
	list := genRangeList(initial)
	list.Remove([2]int{10, 15})
	result := getRangeListValue(list)
	expect := [][2]int{{20, 25}, {30, 35}}
	if !isEqualValue(result, expect) {
		t.Fail()
	}
}

func TestRangeList_RemoveLeftOuter(t *testing.T) {
	initial := [][2]int{{10, 15}, {20, 25}, {30, 35}}
	list := genRangeList(initial)
	list.Remove([2]int{1, 5})
	list.Remove([2]int{17, 19})
	list.Remove([2]int{26, 29})
	result := getRangeListValue(list)
	expect := initial
	if !isEqualValue(result, expect) {
		t.Fail()
	}
}

func TestRangeList_RemoveLeftInner(t *testing.T) {
	initial := [][2]int{{10, 15}, {20, 25}, {30, 35}}
	list := genRangeList(initial)
	list.Remove([2]int{7, 12})
	list.Remove([2]int{20, 22})
	list.Remove([2]int{27, 30})
	result := getRangeListValue(list)
	expect := [][2]int{{12, 15}, {22, 25}, {30, 35}}
	if !isEqualValue(result, expect) {
		t.Fail()
	}
}

func TestRangeList_RemoveInside(t *testing.T) {
	initial := [][2]int{{10, 30}}
	list := genRangeList(initial)
	list.Remove([2]int{17, 22})
	result := getRangeListValue(list)
	expect := [][2]int{{10, 17}, {22, 30}}
	if !isEqualValue(result, expect) {
		t.Fail()
	}
}

func TestRangeList_RemoveRightInner(t *testing.T) {
	initial := [][2]int{{10, 15}, {20, 25}, {30, 35}}
	list := genRangeList(initial)
	list.Remove([2]int{12, 17})
	list.Remove([2]int{25, 27})
	list.Remove([2]int{32, 35})
	result := getRangeListValue(list)
	expect := [][2]int{{10, 12}, {20, 25}, {30, 32}}
	if !isEqualValue(result, expect) {
		t.Fail()
	}
}

func TestRangeList_RemoveRightOuter(t *testing.T) {
	initial := [][2]int{{10, 15}, {20, 25}, {30, 35}}
	list := genRangeList(initial)
	list.Remove([2]int{19, 26})
	result := getRangeListValue(list)
	expect := [][2]int{{10, 15}, {30, 35}}
	if !isEqualValue(result, expect) {
		t.Fail()
	}
	list.Remove([2]int{25, 40})
	result = getRangeListValue(list)
	expect = [][2]int{{10, 15}}
	if !isEqualValue(result, expect) {
		t.Fail()
	}
}

func TestRangeList_RemoveOutside(t *testing.T) {
	initial := [][2]int{{10, 15}, {20, 25}, {30, 35}}
	list := genRangeList(initial)
	list.Remove([2]int{17, 40})
	result := getRangeListValue(list)
	expect := [][2]int{{10, 15}}
	if !isEqualValue(result, expect) {
		t.Fail()
	}
	list.Remove([2]int{1, 20})
	if list.head != nil {
		t.Fail()
	}
}

func TestRangeList_Chaos(t *testing.T) {
	initial := [][2]int{{10, 15}, {20, 25}, {30, 35}}
	list := genRangeList(initial)
	list.Add([2]int{-10, -5})
	list.Remove([2]int{-7, 12})
	list.Add([2]int{-1, 1})
	list.Remove([2]int{24, 32})
	result := getRangeListValue(list)
	expect := [][2]int{{-10, -7}, {-1, 1}, {12, 15}, {20, 24}, {32, 35}}
	if !isEqualValue(result, expect) {
		t.Fail()
	}
}
